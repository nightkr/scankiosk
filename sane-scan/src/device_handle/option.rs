use super::DeviceHandle;
use crate::errors::{self, Error, Result};
use crate::values::{DeviceOptionValue, SaneFixed, SaneUnit, SaneValueType};
use std::ffi::{CStr, CString};
use std::{iter, mem, ptr, slice};

unsafe fn cstr_from_nullable_ptr<'a>(ptr: *const libc::c_char) -> Option<&'a CStr> {
    if ptr.is_null() {
        None
    } else {
        Some(CStr::from_ptr(ptr))
    }
}

/// A streaming `DeviceOption` iterator.
pub struct DeviceOptions<'a, 'backend> {
    device: &'a mut DeviceHandle<'backend>,
    offset: sane_sys::SANE_Int,
    count: sane_sys::SANE_Int,
}

impl<'a, 'backend> DeviceOptions<'a, 'backend> {
    pub(crate) fn new(device: &'a mut DeviceHandle<'backend>) -> Result<Self> {
        let count = device.option_count()?;
        Ok(DeviceOptions {
            device,
            offset: 1,
            count,
        })
    }

    /// Get the next `DeviceOption`.
    ///
    /// Ideally this would be an `Iterator`, but without GATs Rust can't
    /// support generic iterators where the values borrow from the iterator nicely.
    #[allow(clippy::should_implement_trait)]
    pub fn next<'b>(&'b mut self) -> Result<Option<DeviceOption<'b, 'backend>>>
    where
        'a: 'b,
    {
        if self.offset < self.count {
            let offset = self.offset;
            self.offset += 1;
            Ok(Some(self.device.get_option(offset)?))
        } else {
            Ok(None)
        }
    }
}

pub struct DeviceOption<'a, 'backend> {
    device: &'a mut DeviceHandle<'backend>,
    index: sane_sys::SANE_Int,
    raw_descriptor: &'a sane_sys::SANE_Option_Descriptor,
}

impl<'a, 'backend> DeviceOption<'a, 'backend> {
    pub(crate) unsafe fn new(
        device: &'a mut DeviceHandle<'backend>,
        index: sane_sys::SANE_Int,
        raw_descriptor: &'a sane_sys::SANE_Option_Descriptor,
    ) -> Self {
        DeviceOption {
            device,
            index,
            raw_descriptor,
        }
    }

    /// Get a raw option vector from SANE. You probably want to use `get_option_value` instead,
    /// which provides a safe wrapper around this.
    ///
    /// Safety: You are responsible for ensuring that:
    /// - The option at `index` exists
    /// - T matches the option at `index`'s `option_type`
    /// - T doesn't mind getting interpreted from a raw memory value (the `Copy` bound tries to approximate this requirement, but can't guarantee it)
    unsafe fn get_raw_value_vec<T: Copy + Default + 'static>(&self) -> Result<Vec<T>> {
        let vec_bytes = self.size();
        let item_bytes = mem::size_of::<T>();
        assert_eq!(vec_bytes % item_bytes, 0);
        assert_eq!(item_bytes, self.option_type().size());
        let mut buf = iter::repeat(T::default())
            .take(vec_bytes / item_bytes)
            .collect::<Vec<T>>();
        Error::from_sane_status(sane_sys::sane_control_option(
            self.device.raw_handle(),
            self.index,
            sane_sys::SANE_Action_SANE_ACTION_GET_VALUE,
            buf.as_mut_ptr() as *mut libc::c_void,
            ptr::null_mut(),
        ))?;
        Ok(buf)
    }

    pub fn get_value(&self) -> Result<DeviceOptionValue> {
        match self.option_type() {
            SaneValueType::String => {
                let mut buf = unsafe { self.get_raw_value_vec::<sane_sys::SANE_Byte>()? };
                // Remove null bytes since CString::new will re-add its own
                while let Some(0) = buf.last() {
                    buf.pop().unwrap();
                }
                Ok(DeviceOptionValue::String(CString::new(buf).unwrap()))
            }
            SaneValueType::Int => {
                let buf = unsafe { self.get_raw_value_vec::<sane_sys::SANE_Int>()? };
                Ok(DeviceOptionValue::Int(buf))
            }
            SaneValueType::Fixed => {
                let buf = unsafe { self.get_raw_value_vec::<sane_sys::SANE_Fixed>()? };
                let buf = buf.into_iter().map(SaneFixed::from_bits).collect();
                Ok(DeviceOptionValue::Fixed(buf))
            }
            SaneValueType::Bool => {
                assert_eq!(self.size(), mem::size_of::<sane_sys::SANE_Bool>());
                let value = unsafe { self.get_raw_value_vec::<sane_sys::SANE_Bool>()? }[0];
                Ok(DeviceOptionValue::Bool(value as u32 == sane_sys::SANE_TRUE))
            }
            // These types have no meaningful value
            SaneValueType::Button | SaneValueType::Group => Ok(DeviceOptionValue::NoValue),
        }
    }

    pub fn set_automatic_value(&mut self) -> Result<DeviceOptionSetResult> {
        let mut result_flags = 0;
        unsafe {
            Error::from_sane_status(sane_sys::sane_control_option(
                self.device.raw_handle(),
                self.index,
                sane_sys::SANE_Action_SANE_ACTION_SET_AUTO,
                ptr::null_mut(),
                &mut result_flags as *mut sane_sys::SANE_Int,
            ))?;
        }
        Ok(DeviceOptionSetResult::from_bits(result_flags))
    }

    /// Set a raw option vector for a SANE device. You probably want to use `set_option_value` instead,
    /// which provides a safe wrapper around this.
    ///
    /// Safety: You are responsible for ensuring that:
    /// - The option at `index` exists
    /// - T matches the option at `index`'s `option_type`
    /// - `buf` follows the option type's conventions (in particular, strings must be null-terminated)
    /// - nothing else is trying to read `index` at this time
    unsafe fn set_raw_value_vec<T>(&mut self, buf: &mut [T]) -> Result<DeviceOptionSetResult> {
        assert_eq!(self.size(), buf.len() * mem::size_of::<T>());
        let mut result_flags = 0;
        Error::from_sane_status(sane_sys::sane_control_option(
            self.device.raw_handle(),
            self.index,
            sane_sys::SANE_Action_SANE_ACTION_SET_VALUE,
            buf.as_mut_ptr() as *mut libc::c_void,
            &mut result_flags as *mut sane_sys::SANE_Int,
        ))?;
        Ok(DeviceOptionSetResult::from_bits(result_flags))
    }

    pub fn set_value(&mut self, value: DeviceOptionValue) -> Result<DeviceOptionSetResult> {
        match (self.option_type(), value) {
            (SaneValueType::String, DeviceOptionValue::String(value)) => {
                let mut value = value.as_bytes_with_nul().to_vec();
                value.resize(self.size(), 0);
                unsafe { self.set_raw_value_vec::<sane_sys::SANE_Byte>(&mut value) }
            }
            (SaneValueType::Int, DeviceOptionValue::Int(mut values)) => unsafe {
                self.set_raw_value_vec::<sane_sys::SANE_Int>(&mut values)
            },
            (SaneValueType::Fixed, DeviceOptionValue::Fixed(values)) => {
                let mut values = values
                    .into_iter()
                    .map(SaneFixed::to_bits)
                    .collect::<Vec<sane_sys::SANE_Fixed>>();
                unsafe { self.set_raw_value_vec::<sane_sys::SANE_Fixed>(&mut values) }
            }
            (SaneValueType::Bool, DeviceOptionValue::Bool(value)) => unsafe {
                self.set_raw_value_vec::<sane_sys::SANE_Bool>(&mut [value as sane_sys::SANE_Bool])
            },
            (SaneValueType::Button, DeviceOptionValue::NoValue) => unsafe {
                assert_eq!(self.size(), 0);
                self.set_raw_value_vec::<()>(&mut [])
            },
            (SaneValueType::Group, _) => {
                errors::UnwriteableDeviceOption { index: self.index }.fail()
            }
            (expected_type, value) => errors::OptionTypeMismatch {
                expected_type,
                value,
            }
            .fail(),
        }
    }

    pub fn index(&self) -> sane_sys::SANE_Int {
        self.index
    }

    pub fn name(&self) -> Option<&'a CStr> {
        unsafe { cstr_from_nullable_ptr::<'a>(self.raw_descriptor.name) }
    }
    pub fn title(&self) -> &'a CStr {
        unsafe { CStr::from_ptr::<'a>(self.raw_descriptor.title) }
    }
    pub fn desc(&self) -> &'a CStr {
        unsafe { CStr::from_ptr::<'a>(self.raw_descriptor.desc) }
    }
    pub fn option_type(&self) -> SaneValueType {
        SaneValueType::from_repr(self.raw_descriptor.type_)
            .expect("Unknown value type returned from SANE option descriptor")
    }
    pub fn unit(&self) -> SaneUnit {
        SaneUnit::from_repr(self.raw_descriptor.unit)
            .expect("Unknown unit returned from SANE option descriptor")
    }
    pub fn size(&self) -> usize {
        self.raw_descriptor.size as usize
    }
    pub fn count(&self) -> usize {
        match self.option_type() {
            // Buttons and groups don't really have a value
            SaneValueType::Group | SaneValueType::Button => 1,
            // Strings cannot be vectorized, instead the value itself has a dynamic length
            SaneValueType::String => 1,
            tpe => {
                let bytes = self.size();
                let bytes_per_value = tpe.size();
                assert_eq!(bytes % bytes_per_value, 0);
                bytes / bytes_per_value
            }
        }
    }
    pub fn capabilities(&self) -> DeviceOptionCapabilities {
        DeviceOptionCapabilities {
            flags: self.raw_descriptor.cap,
        }
    }
    pub fn constraint(&self) -> Result<DeviceOptionConstraint> {
        match (self.raw_descriptor.constraint_type, self.option_type()) {
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_NONE, SaneValueType::Button) => {
                Ok(DeviceOptionConstraint::Button)
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_NONE, SaneValueType::Bool) => {
                Ok(DeviceOptionConstraint::Bool)
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_NONE, SaneValueType::Int) => {
                Ok(DeviceOptionConstraint::IntAny)
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_NONE, SaneValueType::Fixed) => {
                Ok(DeviceOptionConstraint::FixedAny)
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_NONE, SaneValueType::String) => {
                Ok(DeviceOptionConstraint::StringAny)
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_RANGE, SaneValueType::Int) => {
                let range = unsafe { *self.raw_descriptor.constraint.range };
                Ok(DeviceOptionConstraint::IntRange(StepRange {
                    min: range.min,
                    max: range.max,
                    step: Some(range.quant).filter(|step| *step != 0),
                }))
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_RANGE, SaneValueType::Fixed) => {
                let range = unsafe { *self.raw_descriptor.constraint.range };
                Ok(DeviceOptionConstraint::FixedRange(StepRange {
                    min: SaneFixed::from_bits(range.min),
                    max: SaneFixed::from_bits(range.max),
                    step: Some(range.quant)
                        .filter(|step| *step != 0)
                        .map(SaneFixed::from_bits),
                }))
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_WORD_LIST, SaneValueType::Int) => {
                let whitelist = unsafe {
                    let length_ptr = self.raw_descriptor.constraint.word_list;
                    let start_ptr = length_ptr.offset(1) as *const sane_sys::SANE_Int;
                    slice::from_raw_parts::<'a>(start_ptr, *length_ptr as usize)
                };
                Ok(DeviceOptionConstraint::IntWhitelist(whitelist.to_vec()))
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_WORD_LIST, SaneValueType::Fixed) => {
                let whitelist = unsafe {
                    let length_ptr = self.raw_descriptor.constraint.word_list;
                    let start_ptr = length_ptr.offset(1) as *const SaneFixed;
                    slice::from_raw_parts::<'a>(start_ptr, *length_ptr as usize)
                };
                Ok(DeviceOptionConstraint::FixedWhitelist(whitelist.to_vec()))
            }
            (sane_sys::SANE_Constraint_Type_SANE_CONSTRAINT_STRING_LIST, SaneValueType::String) => {
                let mut whitelist = Vec::new();
                unsafe {
                    let mut next = self.raw_descriptor.constraint.string_list;
                    while !(*next).is_null() {
                        whitelist.push(CStr::from_ptr::<'a>(*next).into());
                        next = next.offset(1);
                    }
                }
                Ok(DeviceOptionConstraint::StringWhitelist(whitelist))
            }
            (_, _) => errors::InvalidConstraintType {
                option_type: self.option_type(),
                constraint_type: self.raw_descriptor.constraint_type,
            }
            .fail(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum DeviceOptionConstraint {
    Button,
    Bool,
    IntAny,
    FixedAny,
    StringAny,
    IntRange(StepRange<i32>),
    FixedRange(StepRange<SaneFixed>),
    IntWhitelist(Vec<i32>),
    FixedWhitelist(Vec<SaneFixed>),
    StringWhitelist(Vec<CString>),
}
// TODO: Add an Into<SaneValueType>?

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct StepRange<T> {
    pub min: T,
    pub max: T,
    pub step: Option<T>,
}

#[derive(Debug, Copy, Clone)]
pub struct DeviceOptionCapabilities {
    flags: sane_sys::SANE_Int,
}

impl DeviceOptionCapabilities {
    pub fn soft_select(self) -> bool {
        (self.flags & sane_sys::SANE_CAP_SOFT_SELECT as sane_sys::SANE_Int) != 0
    }
    pub fn hard_select(self) -> bool {
        (self.flags & sane_sys::SANE_CAP_HARD_SELECT as sane_sys::SANE_Int) != 0
    }
    pub fn soft_detect(self) -> bool {
        (self.flags & sane_sys::SANE_CAP_SOFT_DETECT as sane_sys::SANE_Int) != 0
    }
    pub fn emulated(self) -> bool {
        (self.flags & sane_sys::SANE_CAP_EMULATED as sane_sys::SANE_Int) != 0
    }
    pub fn automatic(self) -> bool {
        (self.flags & sane_sys::SANE_CAP_AUTOMATIC as sane_sys::SANE_Int) != 0
    }
    pub fn inactive(self) -> bool {
        (self.flags & sane_sys::SANE_CAP_INACTIVE as sane_sys::SANE_Int) != 0
    }
    pub fn advanced(self) -> bool {
        (self.flags & sane_sys::SANE_CAP_ADVANCED as sane_sys::SANE_Int) != 0
    }
}

pub struct DeviceOptionSetResult {
    flags: sane_sys::SANE_Int,
}

impl DeviceOptionSetResult {
    pub(crate) fn from_bits(flags: sane_sys::SANE_Int) -> Self {
        DeviceOptionSetResult { flags }
    }

    pub fn inexact(&self) -> bool {
        (self.flags & sane_sys::SANE_INFO_INEXACT as sane_sys::SANE_Int) != 0
    }
    pub fn reload_options(&self) -> bool {
        (self.flags & sane_sys::SANE_INFO_RELOAD_OPTIONS as sane_sys::SANE_Int) != 0
    }
    pub fn reload_params(&self) -> bool {
        (self.flags & sane_sys::SANE_INFO_RELOAD_PARAMS as sane_sys::SANE_Int) != 0
    }
}
