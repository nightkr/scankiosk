use super::DeviceHandle;
use crate::errors::{self, Error, Result, SaneError};
use enum_repr::EnumRepr;
use sane_sys::SANE_Frame;
use snafu::OptionExt;
use std::io::{self, Read};
use std::mem;

pub struct Scan<'scan, 'backend> {
    device: &'scan mut DeviceHandle<'backend>,
}

impl<'scan, 'backend> Scan<'scan, 'backend> {
    pub(crate) unsafe fn new(device: &'scan mut DeviceHandle<'backend>) -> Self {
        Scan { device }
    }

    pub fn get_frame_parameters(&self) -> Result<FrameParameters> {
        let mut raw_params = mem::MaybeUninit::<sane_sys::SANE_Parameters>::zeroed();
        let raw_params = unsafe {
            Error::from_sane_status(sane_sys::sane_get_parameters(
                self.device.handle,
                raw_params.as_mut_ptr(),
            ))?;
            raw_params.assume_init()
        };
        Ok(FrameParameters {
            format: FrameFormat::from_repr(raw_params.format).context(
                errors::UnknownFrameFormat {
                    format: raw_params.format as sane_sys::SANE_Frame,
                },
            )?,
            is_last_frame: raw_params.last_frame == sane_sys::SANE_TRUE as sane_sys::SANE_Bool,
            bytes_per_line: raw_params.bytes_per_line as u32,
            pixels_per_line: raw_params.pixels_per_line as u32,
            lines: Some(raw_params.lines)
                .filter(|lines| *lines != -1)
                .map(|lines| lines as u32),
            depth: raw_params.depth as u8,
        })
    }

    pub fn start_next_frame(&mut self) -> Result<()> {
        if self.get_frame_parameters()?.is_last_frame {
            return errors::AlreadyAtLastFrameOfScan.fail();
        }
        Error::from_sane_status(unsafe { sane_sys::sane_start(self.device.handle) })?;
        Ok(())
    }
}

impl<'scan, 'backend> Read for Scan<'scan, 'backend> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut read_len = 0;
        let read_result = Error::from_sane_status(unsafe {
            sane_sys::sane_read(
                self.device.handle,
                buf.as_mut_ptr(),
                buf.len() as sane_sys::SANE_Int,
                &mut read_len as *mut sane_sys::SANE_Int,
            )
        });
        match read_result {
            Ok(()) => Ok(read_len as usize),
            Err(Error::FromSane {
                cause: SaneError::Eof,
                ..
            }) => Ok(0),
            Err(err) => Err(err.into()),
        }
    }
}

impl<'scan, 'backend> Drop for Scan<'scan, 'backend> {
    fn drop(&mut self) {
        unsafe { sane_sys::sane_cancel(self.device.handle) };
    }
}

#[EnumRepr(type = "SANE_Frame")]
#[derive(Debug, Copy, Clone)]
pub enum FrameFormat {
    Grayscale = sane_sys::SANE_Frame_SANE_FRAME_GRAY,
    Rgb = sane_sys::SANE_Frame_SANE_FRAME_RGB,
    Red = sane_sys::SANE_Frame_SANE_FRAME_RED,
    Green = sane_sys::SANE_Frame_SANE_FRAME_GREEN,
    Blue = sane_sys::SANE_Frame_SANE_FRAME_BLUE,
}

#[derive(Debug, Copy, Clone)]
pub struct FrameParameters {
    pub format: FrameFormat,
    pub is_last_frame: bool,
    pub bytes_per_line: u32,
    pub pixels_per_line: u32,
    pub lines: Option<u32>,
    pub depth: u8,
}
