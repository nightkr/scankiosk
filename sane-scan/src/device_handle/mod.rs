use self::option::*;
use self::scan::*;
use crate::errors::{self, Error, Result};
use crate::values::DeviceOptionValue;
use snafu::OptionExt;
use std::marker::PhantomData;

pub mod option;
pub mod scan;

pub struct DeviceHandle<'backend> {
    _lifetime: PhantomData<&'backend ()>,
    handle: sane_sys::SANE_Handle,
}

impl<'backend> Drop for DeviceHandle<'backend> {
    fn drop(&mut self) {
        unsafe {
            sane_sys::sane_close(self.handle);
        }
    }
}

impl<'backend> DeviceHandle<'backend> {
    /// You probably want to use `SaneBackend::open` instead.
    ///
    /// Safety:
    /// - `handle` must be returned from `sane_open`
    /// - 'backend must *not* be live after `sane_close` has been called (must not outlive the `SaneBackend`)
    pub(crate) unsafe fn from_raw_handle(handle: sane_sys::SANE_Handle) -> Self {
        DeviceHandle {
            _lifetime: PhantomData,
            handle,
        }
    }

    pub(crate) fn raw_handle(&self) -> sane_sys::SANE_Handle {
        self.handle
    }

    pub fn get_option<'a>(
        &'a mut self,
        index: sane_sys::SANE_Int,
    ) -> Result<DeviceOption<'a, 'backend>> {
        unsafe { sane_sys::sane_get_option_descriptor(self.handle, index).as_ref::<'a>() }
            .map(move |raw| unsafe { DeviceOption::new(self, index, raw) })
            .context(errors::UnknownDeviceOptionIndex { index })
    }

    pub fn options<'a>(&'a mut self) -> Result<DeviceOptions<'a, 'backend>> {
        DeviceOptions::new(self)
    }

    pub fn option_count(&mut self) -> Result<sane_sys::SANE_Int> {
        // Option 0 is a special meta-option that contains the number of options (including the meta-option)
        let count = self.get_option(0)?.get_value()?;
        match count {
            DeviceOptionValue::Int(values) => {
                if values.len() == 1 {
                    Ok(values[0])
                } else {
                    panic!("Invalid count value length: {:?}", values)
                }
            }
            invalid_count => panic!("Invalid count type: {:?}", invalid_count),
        }
    }

    pub fn start_scan<'scan>(&'scan mut self) -> Result<Scan<'scan, 'backend>> {
        Error::from_sane_status(unsafe { sane_sys::sane_start(self.handle) })?;
        Ok(unsafe { Scan::new(self) })
    }
}
