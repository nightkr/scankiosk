use crate::errors::{self, Result};
use enum_repr::EnumRepr;
use sane_sys::{SANE_Unit, SANE_Value_Type};
use std::ffi::CString;
use std::mem;

#[EnumRepr(type = "SANE_Value_Type")]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SaneValueType {
    Bool = sane_sys::SANE_Value_Type_SANE_TYPE_BOOL,
    Int = sane_sys::SANE_Value_Type_SANE_TYPE_INT,
    Fixed = sane_sys::SANE_Value_Type_SANE_TYPE_FIXED,
    String = sane_sys::SANE_Value_Type_SANE_TYPE_STRING,
    Button = sane_sys::SANE_Value_Type_SANE_TYPE_BUTTON,
    Group = sane_sys::SANE_Value_Type_SANE_TYPE_GROUP,
}

impl SaneValueType {
    pub fn size(self) -> usize {
        use SaneValueType as T;
        match self {
            T::Bool => mem::size_of::<sane_sys::SANE_Bool>(),
            T::Int => mem::size_of::<sane_sys::SANE_Int>(),
            T::Fixed => mem::size_of::<sane_sys::SANE_Fixed>(),
            T::String => mem::size_of::<sane_sys::SANE_Char>(),
            T::Button | T::Group => 0,
        }
    }
}

#[EnumRepr(type = "SANE_Unit")]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SaneUnit {
    None = sane_sys::SANE_Unit_SANE_UNIT_NONE,
    Pixel = sane_sys::SANE_Unit_SANE_UNIT_PIXEL,
    Bit = sane_sys::SANE_Unit_SANE_UNIT_BIT,
    Mm = sane_sys::SANE_Unit_SANE_UNIT_MM,
    Dpi = sane_sys::SANE_Unit_SANE_UNIT_DPI,
    Percent = sane_sys::SANE_Unit_SANE_UNIT_PERCENT,
    Microsecond = sane_sys::SANE_Unit_SANE_UNIT_MICROSECOND,
}

pub type SaneFixed = fixed::FixedI32<fixed::frac::U16>;

pub mod vector_mode {
    use super::SaneFixed;
    use std::fmt::Debug;

    pub trait VectorMode {
        type Int: Debug + Clone + PartialEq + Eq;
        type Fixed: Debug + Clone + PartialEq + Eq;
    }

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct Single;

    impl VectorMode for Single {
        type Int = i32;
        type Fixed = SaneFixed;
    }

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct Vectored;

    impl VectorMode for Vectored {
        type Int = Vec<i32>;
        type Fixed = Vec<SaneFixed>;
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum DeviceOptionValue<Mode: vector_mode::VectorMode = vector_mode::Vectored> {
    String(CString),
    Int(Mode::Int),
    Fixed(Mode::Fixed),
    Bool(bool),
    NoValue,
}

impl DeviceOptionValue {
    pub fn set_field(
        &mut self,
        field: usize,
        new: DeviceOptionValue<vector_mode::Single>,
    ) -> Result<()> {
        use DeviceOptionValue as V;
        match (self, new) {
            (V::String(ref mut old), V::String(ref new)) => *old = new.clone(),
            (V::Int(ref mut old), V::Int(new)) => old[field] = new,
            (V::Fixed(ref mut old), V::Fixed(new)) => old[field] = new,
            (V::Bool(ref mut old), V::Bool(new)) => *old = new,
            (V::NoValue, V::NoValue) => {}
            (old, new) => {
                return errors::UpdateValueTypeMismatch {
                    old: old.clone(),
                    new,
                }
                .fail()
            }
        }
        Ok(())
    }
}
