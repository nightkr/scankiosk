use std::ffi::CStr;

/// Safety: `Devices` does *not* own `devices`, instead it is owned by the SANE backend and exists until the next `sane_get_devices` (``SaneBackend::get_devices`) call.
/// This is checked by `SaneBackend::get_devices` requiring a mut borrow of `SaneBackend`, while `Devices` keeps a phantom regular borrow.
/// `DeviceInfo` is also covered by this requirement, since its inner reference is also owned by the `sane_get_devices` call.
pub(crate) struct Devices<'a> {
    /// Null-terminated C array of devices
    /// The array itself has the same lifetime as the individual items.
    devices: *const &'a sane_sys::SANE_Device,
    offset: isize,
}

impl<'a> Devices<'a> {
    pub(crate) fn from_c_array(devices: *const &'a sane_sys::SANE_Device) -> Self {
        Devices { devices, offset: 0 }
    }
}

impl<'a> Iterator for Devices<'a> {
    type Item = DeviceInfo<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let next =
            unsafe { self.devices.offset(self.offset).as_ref() }.map(|inner| DeviceInfo { inner });
        if next.is_some() {
            self.offset += 1;
        }
        next
    }
}

#[derive(Debug)]
pub struct DeviceInfo<'a> {
    inner: &'a sane_sys::SANE_Device,
}

impl<'a> DeviceInfo<'a> {
    pub fn name(&self) -> &'a CStr {
        unsafe { CStr::from_ptr::<'a>(self.inner.name) }
    }
    pub fn vendor(&self) -> &'a CStr {
        unsafe { CStr::from_ptr::<'a>(self.inner.vendor) }
    }
    pub fn model(&self) -> &'a CStr {
        unsafe { CStr::from_ptr::<'a>(self.inner.model) }
    }
    pub fn device_type(&self) -> &'a CStr {
        unsafe { CStr::from_ptr::<'a>(self.inner.type_) }
    }
}
