use std::fmt::{self, Display, Formatter};

#[derive(Debug, Copy, Clone)]
pub struct Version {
    code: i32,
}

impl Version {
    pub fn from_code(code: i32) -> Self {
        Version { code }
    }

    pub fn code(self) -> i32 {
        self.code
    }

    pub fn major(self) -> i32 {
        unsafe { sane_sys::sanev_version_major(self.code) }
    }

    pub fn minor(self) -> i32 {
        unsafe { sane_sys::sanev_version_minor(self.code) }
    }

    pub fn build(self) -> i32 {
        unsafe { sane_sys::sanev_version_build(self.code) }
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}.{}.{}", self.major(), self.minor(), self.build())
    }
}
