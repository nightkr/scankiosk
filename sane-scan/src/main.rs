use sane_scan::values::DeviceOptionValue;
use sane_scan::*;
use std::ffi::CString;
use std::io::Read;

fn run_main() -> Result<()> {
    let mut sane = SaneBackend::init()?;
    let mut chosen_name = CString::new(b"0".to_vec()).unwrap();
    for dev in sane.get_devices(true)? {
        dbg!((dev.name(), dev.model(), dev.vendor()));
        chosen_name = dev.name().to_owned();
    }
    let mut device = sane.open(dbg!(&chosen_name))?;
    dbg!(device.option_count()?);
    let mut options = device.options()?;
    while let Some(mut opt) = options.next()? {
        dbg!(
            (
                opt.index(),
                opt.name(),
                opt.title(),
                opt.option_type(),
                opt.capabilities(),
                opt.constraint()?
            ),
            opt.get_value()
        );
        // opt.set_value(DeviceOptionValue::Int(vec![5]))?;
    }
    let mut scan = device.start_scan()?;
    let mut frames = 0;
    loop {
        let frame_params = scan.get_frame_parameters()?;
        dbg!(frame_params);
        let mut frame_buf = Vec::<u8>::new();
        scan.read_to_end(&mut frame_buf).unwrap();
        image::save_buffer(
            format!("scan_{}.png", frames),
            &frame_buf,
            frame_params.pixels_per_line,
            frame_buf.len() as u32 / frame_params.bytes_per_line,
            image::Gray(frame_params.depth),
        )
        .unwrap();
        frames += 1;
        if frame_params.is_last_frame {
            break;
        }
    }
    dbg!(frames);
    Ok(())
}

fn main() {
    let result = run_main();
    if let Err(ref err) = result {
        println!("Error: {}", err);
    }
    result.unwrap();
}
