use self::device_handle::DeviceHandle;
use self::device_metadata::{DeviceInfo, Devices};
use self::version::Version;
use lazy_static::lazy_static;
use snafu::ensure;
use std::ffi::CStr;
use std::marker::PhantomData;
use std::ptr;
use std::sync::{Mutex, MutexGuard};

pub use crate::errors::{Error, Result};

pub mod device_handle;
pub mod device_metadata;
pub mod errors;
pub mod values;
pub mod version;

lazy_static! {
    static ref BACKEND_LOCK: Mutex<()> = Mutex::new(());
}

pub struct SaneBackend {
    // Outside construction is disallowed, since SANE requires special initialization
    _private: PhantomData<()>,
    // !Sync, since the SANE backend relies on storing global state
    // !Send, since we don't know whether the SANE backend uses any thread-local storage
    _unsync: PhantomData<*mut ()>,
    _singleton_lock: MutexGuard<'static, ()>,
    version: Version,
}

impl SaneBackend {
    /// Initializes the SANE backend.
    ///
    /// There can only be a single `SaneBackend` at any time, since the SANE C API relies
    /// on internally managed global state. This is enforced by an internal mutex lock.
    pub fn init() -> Result<Self> {
        let singleton_lock = BACKEND_LOCK.lock().unwrap();
        let mut version_code = 0;
        Error::from_sane_status(unsafe { sane_sys::sane_init(&mut version_code as *mut _, None) })?;
        let version = Version::from_code(version_code);
        ensure!(
            version.major() == 1,
            errors::UnknownBackendVersion { version }
        );
        Ok(SaneBackend {
            _private: PhantomData,
            _unsync: PhantomData,
            _singleton_lock: singleton_lock,
            version,
        })
    }

    pub fn version(&self) -> Version {
        self.version
    }

    pub fn get_devices<'a>(
        &'a mut self,
        local_only: bool,
    ) -> Result<impl Iterator<Item = DeviceInfo<'a>>> {
        let mut devices: *mut *const sane_sys::SANE_Device = ptr::null_mut();
        Error::from_sane_status(unsafe {
            sane_sys::sane_get_devices(&mut devices as *mut _, local_only.into())
        })?;
        Ok(Devices::from_c_array(devices as *const &'a _))
    }

    pub fn open<'a>(&'a self, name: &CStr) -> Result<DeviceHandle<'a>> {
        let mut handle: sane_sys::SANE_Handle = ptr::null_mut();
        Error::from_sane_status(unsafe {
            sane_sys::sane_open(name.as_ptr(), &mut handle as *mut _)
        })?;
        Ok(unsafe { DeviceHandle::from_raw_handle(handle) })
    }
}

impl Drop for SaneBackend {
    fn drop(&mut self) {
        unsafe {
            sane_sys::sane_exit();
        }
    }
}

#[test]
fn test_backend_must_be_singleton() {
    use std::sync::TryLockError;

    // Make sure that the lock is initially available
    let _ = BACKEND_LOCK.lock().unwrap();
    let backend = SaneBackend::init().unwrap();
    match BACKEND_LOCK.try_lock() {
        Err(TryLockError::WouldBlock) => {}
        result => panic!("SaneBackend did not lock itself correctly: {:?}", result),
    };
    drop(backend);
    // The lock should be available again now that the backend was dropped
    let _ = BACKEND_LOCK.lock().unwrap();
}
