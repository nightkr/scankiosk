use crate::values::{vector_mode, DeviceOptionValue, SaneValueType};
use crate::version::Version;
use enum_repr::EnumRepr;
use sane_sys::SANE_Status;
use snafu::{Backtrace, Snafu};
use std::convert::From;
use std::ffi::CStr;
use std::fmt::{self, Display, Formatter};
use std::io;

#[EnumRepr(type = "SANE_Status")]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SaneError {
    Unsupported = sane_sys::SANE_Status_SANE_STATUS_UNSUPPORTED,
    Cancelled = sane_sys::SANE_Status_SANE_STATUS_CANCELLED,
    DeviceBusy = sane_sys::SANE_Status_SANE_STATUS_DEVICE_BUSY,
    Inval = sane_sys::SANE_Status_SANE_STATUS_INVAL,
    Eof = sane_sys::SANE_Status_SANE_STATUS_EOF,
    Jammed = sane_sys::SANE_Status_SANE_STATUS_JAMMED,
    NoDocs = sane_sys::SANE_Status_SANE_STATUS_NO_DOCS,
    CoverOpen = sane_sys::SANE_Status_SANE_STATUS_COVER_OPEN,
    IoError = sane_sys::SANE_Status_SANE_STATUS_IO_ERROR,
    NoMem = sane_sys::SANE_Status_SANE_STATUS_NO_MEM,
    AccessDenied = sane_sys::SANE_Status_SANE_STATUS_ACCESS_DENIED,
}

impl SaneError {
    /// Safety: the returned `CStr` is only valid until the next call to `describe`. This function
    /// is for internal use only, use `SaneError::fmt` instead.
    unsafe fn describe(self) -> &'static CStr {
        CStr::from_ptr(sane_sys::sane_strstatus(self.repr()))
    }
}

impl Display for SaneError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        // msg is valid until the next call to sane_strstatus/describe
        // we're safe for single-threaded applications since Formatter::write_str copies the string ASAP,
        // but this can theoretically still cause race conditions if multiple threads try to fmt() at the same time
        // However, in practice this is not an issue since SANE just has a bunch of hard-coded string constants.
        let msg = unsafe { self.describe() };
        f.write_str(&msg.to_string_lossy())
    }
}

impl From<SaneError> for io::ErrorKind {
    fn from(err: SaneError) -> Self {
        match err {
            SaneError::Unsupported => io::ErrorKind::Other,
            SaneError::Cancelled => io::ErrorKind::Interrupted,
            SaneError::DeviceBusy => io::ErrorKind::AddrInUse,
            SaneError::Inval => io::ErrorKind::InvalidInput,
            SaneError::Eof => io::ErrorKind::UnexpectedEof,
            SaneError::Jammed => io::ErrorKind::Other,
            SaneError::NoDocs => io::ErrorKind::Other,
            SaneError::CoverOpen => io::ErrorKind::InvalidInput,
            SaneError::IoError => io::ErrorKind::Other,
            SaneError::NoMem => io::ErrorKind::Other,
            SaneError::AccessDenied => io::ErrorKind::PermissionDenied,
        }
    }
}

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("internal SANE error: {}", cause))]
    FromSane {
        cause: SaneError,
        backtrace: Backtrace,
    },
    #[snafu(display("unknown SANE backend version: {}", version))]
    #[snafu(visibility(pub(crate)))]
    UnknownBackendVersion {
        version: Version,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "option type mismatch, expected {:?} but got {:?}",
        expected_type,
        value
    ))]
    #[snafu(visibility(pub(crate)))]
    OptionTypeMismatch {
        expected_type: SaneValueType,
        value: DeviceOptionValue,
        backtrace: Backtrace,
    },
    #[snafu(display("unknown device option index: {}", index))]
    #[snafu(visibility(pub(crate)))]
    UnknownDeviceOptionIndex {
        index: sane_sys::SANE_Int,
        backtrace: Backtrace,
    },
    #[snafu(display("unwriteable device option: {}", index))]
    #[snafu(visibility(pub(crate)))]
    UnwriteableDeviceOption {
        index: sane_sys::SANE_Int,
        backtrace: Backtrace,
    },
    #[snafu(display("invalid constraint for type {:?}: {:?}", option_type, constraint_type))]
    #[snafu(visibility(pub(crate)))]
    InvalidConstraintType {
        option_type: SaneValueType,
        constraint_type: sane_sys::SANE_Constraint_Type,
        backtrace: Backtrace,
    },
    #[snafu(display("unknown frame format: {:?}", format))]
    #[snafu(visibility(pub(crate)))]
    UnknownFrameFormat {
        format: sane_sys::SANE_Frame,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "tried to update update value {:?} with new value of mismatching type {:?}",
        old,
        new
    ))]
    #[snafu(visibility(pub(crate)))]
    UpdateValueTypeMismatch {
        old: DeviceOptionValue,
        new: DeviceOptionValue<vector_mode::Single>,
        backtrace: Backtrace,
    },
    #[snafu(display("already at last frame of the scan"))]
    #[snafu(visibility(pub(crate)))]
    AlreadyAtLastFrameOfScan {
        backtrace: Backtrace,
    }
}

impl Error {
    pub fn from_sane_status(status: SANE_Status) -> Result<()> {
        match status {
            sane_sys::SANE_Status_SANE_STATUS_GOOD => Ok(()),
            _ => FromSane {
                cause: SaneError::from_repr(status)
                    .unwrap_or_else(move || panic!("Unknown SANE error code: {}", status)),
            }
            .fail(),
        }
    }
}

impl From<Error> for io::Error {
    fn from(err: Error) -> Self {
        let kind = match err {
            Error::FromSane { cause, .. } => cause.into(),
            _ => io::ErrorKind::Other,
        };
        Self::new(kind, err)
    }
}

pub type Result<T, E = Error> = std::result::Result<T, E>;
