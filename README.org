Goal: An "all in one" scanning application to be used in a "kiosk" environment by mutually untrusting users.

Sub-goals:

- Allow users to send their scanned documents to another device or an email address without exposing any private credentials
- Never write *anything* to disk, it should work fine even if all available disks are mounted read-only
  - "The easiest way to never expose private information is to never store it."  - Me

* Running

There is currently no usable GUI, but sane-scan (my Rust SANE bindings) has a simple testing utility that can be started by 
executing ~cargo run --bin sane-scan~.

Note: Arch's ~sane~ package currently enables the broken ~as6e~ backend by default. If you get the following error message:

#+begin_src text
     Running `target/debug/sane-scan`
*** stack smashing detected ***: <unknown> terminated
fish: “cargo run --bin sane-scan” terminated by signal SIGABRT (Abort)
#+end_src

then you've probably run into this issue. This can be fixed by commenting out the ~as6e~ line from [[/etc/sane.d/dll.conf]].

For testing purposes you might also want to force a particular backend. To do this you can either replace the contents of
[[/etc/sane.d/dll.conf]] with ~test~, or export ~LD_PRELOAD=/usr/lib64/sane/libsane-test.so~ (the exact path may vary between
distros).

* Licensing

- ~sane-sys~ is a mechanically generated binding to [[http://www.sane-project.org/html/][the SANE 1.0 standard]], so it should not meet any reasonable threshold
  of originality. The SANE API is public domain, so consider that true for ~sane-sys~ as well. I've listed it as CC0, since
  not all countries recognize explicit PD declarations.

  - Keep in mind the specific SANE backend that you use may be under a different license. Most official backends are 
    [[http://sane-project.org/license.html][licensed]] under the [[http://sane-project.org/COPYING][GPLv2]] + their own SANE exception that limits virality (similar in spirit to the LGPL), but make sure
    to check with the backend that you end up using (both the device backend and any meta backends used, such as ~libsane-dll~).

- ~sane-scan~ and ~scankiosk-ui~ are licensed under the [[./LICENSE][AGPLv3]] or newer.
