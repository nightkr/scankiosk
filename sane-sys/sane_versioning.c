#include "sane_versioning.h"

SANE_Int sanev_version_code(SANE_Int major, SANE_Int minor, SANE_Int build) {
  return SANE_VERSION_CODE(major, minor, build);
}
SANE_Int sanev_version_major(SANE_Int version_code) {
  return SANE_VERSION_MAJOR(version_code);
}
SANE_Int sanev_version_minor(SANE_Int version_code) {
  return SANE_VERSION_MINOR(version_code);
}
SANE_Int sanev_version_build(SANE_Int version_code) {
  return SANE_VERSION_BUILD(version_code);
}
