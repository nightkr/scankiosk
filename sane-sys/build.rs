use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-link-lib=sane");
    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .generate()
        .expect("Unable to generate bindings");
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

    cc::Build::new()
        .file("sane_versioning.c")
        .compile("sane_versioning");
}
