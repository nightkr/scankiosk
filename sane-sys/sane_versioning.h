#include <sane/sane.h>

#ifndef __SANE_VERSIONING_H
#define __SANE_VERSIONING_H

SANE_Int sanev_version_code(SANE_Int major, SANE_Int minor, SANE_Int build);
SANE_Int sanev_version_major(SANE_Int version_code);
SANE_Int sanev_version_minor(SANE_Int version_code);
SANE_Int sanev_version_build(SANE_Int version_code);

#endif
