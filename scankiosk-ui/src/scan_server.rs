use futures::channel::{mpsc as mpsc_f, oneshot as oneshot_f};
use futures::prelude::*;
use sane_scan::device_handle::option::DeviceOptionConstraint;
use sane_scan::device_handle::scan::FrameParameters;
use sane_scan::values::*;
use sane_scan::*;
use std::io::Read;
use std::sync::mpsc;
use std::thread;

/// Ref-counted client handle to an off-thread scanner backend
#[derive(Debug, Clone)]
pub struct ScanClient {
    cmd_tx: mpsc::Sender<ScannerCmd>,
}

impl ScanClient {
    pub fn subscribe_to_options(&self) -> impl Stream<Item = OptionDescriptor> {
        let (opt_tx, opt_rx) = mpsc_f::unbounded();
        self.cmd_tx
            .send(ScannerCmd::SubscribeToOptions { opt_tx })
            .unwrap();
        opt_rx
    }

    pub fn set_option_value(
        &self,
        option_index: i32,
        option_field: usize,
        value: DeviceOptionValue<vector_mode::Single>,
    ) {
        self.cmd_tx
            .send(ScannerCmd::SetOptionValue {
                option_index,
                option_field,
                value,
            })
            .unwrap();
    }

    pub fn start_scan(&self) -> impl Stream<Item = (FrameParameters, impl Stream<Item = Vec<u8>>)> {
        let (frame_tx, frame_rx) = mpsc_f::unbounded();
        self.cmd_tx.send(ScannerCmd::Scan { frame_tx }).unwrap();
        frame_rx
    }
}

enum ScannerCmd {
    SubscribeToOptions {
        opt_tx: mpsc_f::UnboundedSender<OptionDescriptor>,
    },
    SetOptionValue {
        option_index: i32,
        option_field: usize,
        value: DeviceOptionValue<vector_mode::Single>,
    },
    Scan {
        frame_tx: mpsc_f::UnboundedSender<(FrameParameters, mpsc_f::UnboundedReceiver<Vec<u8>>)>,
    },
}

pub fn start() -> ScanClient {
    let (cmd_tx, cmd_rx) = mpsc::channel();
    thread::spawn(move || run_scan_server(cmd_rx));
    ScanClient { cmd_tx }
}

fn run_scan_server(cmd_rx: mpsc::Receiver<ScannerCmd>) {
    let mut sane = SaneBackend::init().unwrap();
    let device_meta = sane.get_devices(true).unwrap().next().unwrap();
    let device_name = device_meta.name().to_owned();
    let mut device = sane.open(&device_name).unwrap();

    let mut opt_subscriber_tx: Option<mpsc_f::UnboundedSender<OptionDescriptor>> = None;

    let list_options = |device: &mut sane_scan::device_handle::DeviceHandle,
                        opt_tx: &mpsc_f::UnboundedSender<OptionDescriptor>| {
        let mut opts = device.options().unwrap();
        while let Some(opt) = opts.next().unwrap() {
            if opt.option_type() == SaneValueType::Group {
                continue;
            }
            opt_tx.unbounded_send(OptionDescriptor::new(&opt)).unwrap();
        }
    };

    for cmd in cmd_rx {
        match cmd {
            ScannerCmd::SubscribeToOptions { opt_tx } => {
                dbg!("listing options");
                list_options(&mut device, &opt_tx);
                opt_subscriber_tx = Some(opt_tx);
            }
            ScannerCmd::SetOptionValue {
                option_index,
                option_field,
                value,
            } => {
                let mut opt = device.get_option(option_index).unwrap();
                let mut value_vec = opt.get_value().unwrap();
                value_vec.set_field(option_field, value).unwrap();
                opt.set_value(value_vec).unwrap();
                if let Some(ref opt_tx) = &opt_subscriber_tx {
                    list_options(&mut device, opt_tx);
                }
            }
            ScannerCmd::Scan { frame_tx } => {
                let mut scan = device.start_scan().unwrap();
                'frames: loop {
                    let frame_params = scan.get_frame_parameters().unwrap();
                    let (frame_buf_tx, frame_buf_rx) = mpsc_f::unbounded();
                    frame_tx
                        .unbounded_send((frame_params, frame_buf_rx))
                        .unwrap();
                    let mut buf = [0; 4096];

                    'frame_bufs: loop {
                        let size = scan.read(&mut buf).unwrap();
                        if size == 0 {
                            break;
                        }
                        frame_buf_tx.unbounded_send(buf[..size].to_owned()).unwrap();
                    }

                    // TODO: Detect this in a nicer way
                    if let Err(_) = scan.start_next_frame() {
                        break 'frames;
                    }
                }
            }
        }
    }
}

/// A static variant of sane-scan's `DeviceOption` where all values are already resolved
#[derive(Debug)]
pub struct OptionDescriptor {
    pub index: i32,
    pub title: String,
    pub constraint: DeviceOptionConstraint,
    pub count: usize,
    pub value: sane_scan::Result<DeviceOptionValue>,
    pub capabilities: sane_scan::device_handle::option::DeviceOptionCapabilities,
}

impl OptionDescriptor {
    fn new(opt: &sane_scan::device_handle::option::DeviceOption) -> Self {
        OptionDescriptor {
            index: opt.index(),
            title: opt.title().to_string_lossy().into_owned(),
            constraint: opt.constraint().unwrap(),
            count: opt.count(),
            value: opt.get_value(),
            capabilities: opt.capabilities(),
        }
    }
}
