use crate::scan_server::ScanClient;
use futures::prelude::*;
use gdk_pixbuf::Pixbuf;
use sane_scan::device_handle::scan::FrameFormat;

pub fn scan_to_pixbuf_stream(scanner: &ScanClient) -> impl Stream<Item = Pixbuf> {
    let frames_rx = scanner.start_scan();
    let mut pixbuf: Option<Pixbuf> = None;
    frames_rx
        .map(move |(frame_params, frame_buf_rx)| {
            let (subpixel_offset, subpixel_stride, subpixel_copies) = match frame_params.format {
                FrameFormat::Grayscale => (0, 1, 3),
                FrameFormat::Rgb => (0, 1, 1),
                FrameFormat::Red => (0, 3, 1),
                FrameFormat::Green => (1, 3, 1),
                FrameFormat::Blue => (2, 3, 1),
            };
            let bytes_per_subpixel = (frame_params.depth / 8) as usize;
            let byte_offset = subpixel_offset * bytes_per_subpixel;
            let byte_stride = subpixel_stride * bytes_per_subpixel;

            let pixbuf = pixbuf
                .get_or_insert_with(|| {
                    let pixbuf = Pixbuf::new(
                        gdk_pixbuf::Colorspace::Rgb,
                        false,
                        frame_params.depth as i32,
                        frame_params.pixels_per_line as i32,
                        frame_params.lines.unwrap() as i32,
                    )
                    .unwrap();
                    pixbuf.fill(0xffffffff);
                    pixbuf
                })
                .clone();

            let src_buf_row_stride = (frame_params.bytes_per_line * subpixel_copies) as usize;
            let target_buf_row_stride = pixbuf.get_rowstride() as usize;
            let row_stride_delta = target_buf_row_stride - (src_buf_row_stride * subpixel_stride);
            let mut curr_subpixel = 0;
            let mut row_stride_offset = 0;
            frame_buf_rx.map(move |buf| {
                let target_buf = unsafe { pixbuf.get_pixels() };
                for subpixel in buf.chunks(bytes_per_subpixel.into()) {
                    for _subpixel_copy_i in 0..subpixel_copies {
                        let subpixel_byte_pos =
                            curr_subpixel * byte_stride + byte_offset + row_stride_offset;
                        target_buf[subpixel_byte_pos..subpixel_byte_pos + bytes_per_subpixel]
                            .copy_from_slice(subpixel);
                        curr_subpixel += 1;
                        if (curr_subpixel * bytes_per_subpixel) % src_buf_row_stride == 0 {
                            row_stride_offset += row_stride_delta;
                        }
                    }
                }
                pixbuf.clone()
            })
        })
        .flatten()
}
