use crate::scan_server::OptionDescriptor;
use futures::prelude::*;
use gtk::prelude::*;
use gtk::Inhibit;
use sane_scan::device_handle::option::DeviceOptionConstraint;
use sane_scan::values::*;
use std::cell::RefCell;
use std::collections::HashMap;
use std::ffi::CString;
use std::rc::Rc;

mod scan_gtk;
mod scan_server;

fn main() {
    let scanner = scan_server::start();

    gtk::init().unwrap();
    let ctx = glib::MainContext::default();
    let glade_src = include_str!("scan-window.glade");
    let builder = gtk::Builder::new_from_string(glade_src);
    let window: gtk::Assistant = builder.get_object("main-window").unwrap();
    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });
    window.show_all();

    let options: gtk::Grid = builder.get_object("options-list").unwrap();
    let option_fields: RefCell<HashMap<i32, Vec<gtk::Widget>>> = RefCell::new(HashMap::new());
    let mut option_row = 0;

    let is_updating = Rc::new(RefCell::new(false));
    let scanner2 = scanner.clone();
    ctx.spawn_local(scanner.subscribe_to_options().for_each(move |opt| {
        let mut option_fields = option_fields.borrow_mut();
        let opt_fields = option_fields.entry(opt.index).or_insert_with(|| {
            let opt_lbl = gtk::Label::new(Some(&opt.title));
            opt_lbl.set_visible(true);
            options.attach(&opt_lbl, 0, option_row, 1, 1);
            let opt_box = gtk::Box::new(gtk::Orientation::Vertical, 0);
            opt_box.set_visible(true);
            options.attach(&opt_box, 1, option_row, 1, 1);
            option_row += 1;
            let mut opt_fields = Vec::new();
            for opt_field_i in 0..opt.count {
                let option_index = opt.index;
                let scanner = scanner2.clone();
                let is_updating = is_updating.clone();
                let opt_field = option_widget(&opt, move |value| {
                    if !*is_updating.borrow() {
                        scanner.set_option_value(option_index, opt_field_i, value);
                    }
                });
                opt_field.set_visible(true);
                opt_box.pack_start(&opt_field, false, false, 0);
                opt_fields.push(opt_field);
            }
            opt_fields
        });
        update_option_widget_state(opt_fields, &opt);
        if let Ok(value) = opt.value {
            *is_updating.borrow_mut() = true;
            option_widget_read_value(opt_fields, value);
            *is_updating.borrow_mut() = false;
        }
        future::ready(())
    }));

    let window2 = window.clone();
    let scan_btn: gtk::Button = builder.get_object("scan-btn").unwrap();
    let scan_preview: gtk::Image = builder.get_object("scan-preview").unwrap();
    let scan_page: gtk::Box = builder.get_object("scan-page").unwrap();
    scan_btn.connect_clicked(move |_| {
        let scan_preview = scan_preview.clone();
        let window = window2.clone();
        let scan_page = scan_page.clone();
        window.set_page_complete(&scan_page, false);
        ctx.spawn_local(
            scan_gtk::scan_to_pixbuf_stream(&scanner)
                .for_each(move |pixbuf| {
                    scan_preview.set_from_pixbuf(Some(&pixbuf));
                    future::ready(())
                })
                .inspect(move |()| window.set_page_complete(&scan_page, true)),
        );
    });

    let upload_page: gtk::Box = builder.get_object("upload-page").unwrap();
    window.connect_prepare(move |_, page| {
        if page.eq(&upload_page) {
            let ffsend_client = ffsend_api::client::ClientConfig::default().client(true);
            // ffsend_api::action::upload::Upload::new(version: Version, host: Url, path: PathBuf, name: Option<String>, password: Option<String>, params: Option<ParamsData>)
        }
    });

    gtk::main();
}

#[repr(u32)]
enum WhitelistWidgetColumns {
    Index = 0,
    Text,
}

fn update_option_widget_state(widgets: &[gtk::Widget], opt: &OptionDescriptor) {
    for widget in widgets {
        widget.set_sensitive(opt.capabilities.soft_select() && !opt.capabilities.inactive());
    }
}

fn option_widget_whitelist<I: IntoIterator<Item = String>, OnUpdate: 'static + Fn(usize)>(
    whitelist: I,
    on_update: OnUpdate,
) -> gtk::Widget {
    let model = gtk::ListStore::new(&[u32::static_type(), I::Item::static_type()]);
    for (i, choice) in (0..).zip(whitelist) {
        model.insert_with_values(
            None,
            &[
                WhitelistWidgetColumns::Index as u32,
                WhitelistWidgetColumns::Text as u32,
            ],
            &[&i, &choice],
        );
    }
    let view = gtk::ComboBoxBuilder::new()
        .id_column(WhitelistWidgetColumns::Text as i32)
        .model(&model.upcast())
        .build();
    let col_renderer = gtk::CellRendererText::new();
    view.pack_start(&col_renderer, false);
    view.add_attribute(&col_renderer, "text", WhitelistWidgetColumns::Text as i32);
    view.connect_changed(move |view| {
        on_update(
            view.get_model()
                .unwrap()
                .get_value(
                    &view.get_active_iter().unwrap(),
                    WhitelistWidgetColumns::Index as i32,
                )
                .downcast::<u32>()
                .unwrap()
                .get()
                .unwrap() as usize,
        )
    });
    view.upcast()
}

fn option_widget<OnUpdate: 'static + Fn(DeviceOptionValue<vector_mode::Single>)>(
    opt: &OptionDescriptor,
    on_update: OnUpdate,
) -> gtk::Widget {
    use DeviceOptionConstraint as C;
    match &opt.constraint {
        C::Button => {
            let widget = gtk::ButtonBuilder::new().label("Do eet").build();
            widget.connect_clicked(move |_| on_update(DeviceOptionValue::NoValue));
            widget.upcast()
        }
        C::Bool => {
            let widget = gtk::CheckButton::new();
            widget.connect_toggled(move |widget| on_update(DeviceOptionValue::Bool(widget.get_active())));
            widget.upcast()
        }
        C::IntAny => {
            let widget = gtk::SpinButton::new(None as Option<&gtk::Adjustment>, 1.0, 0);
            // TODO: Find a better event to hook up to
            widget.connect_activate(move |widget| on_update(DeviceOptionValue::Int(widget.get_value_as_int())));
            widget.upcast()
        }
        C::FixedAny => {
            let widget = gtk::SpinButton::new(None as Option<&gtk::Adjustment>, 1.0, 2);
            // TODO: Find a better event to hook up to
            widget.connect_activate(move |widget| on_update(DeviceOptionValue::Fixed(SaneFixed::from_num(widget.get_value()))));
            widget.upcast()
        }
        C::StringAny => {
            let widget = gtk::Entry::new();
            // TODO: Find a better event to hook up to
            widget.connect_activate(move |widget| on_update(DeviceOptionValue::String(CString::new(widget.get_text().unwrap().as_str()).unwrap())));
            widget.upcast()
        }
        C::IntRange(range) => {
            let widget = gtk::Scale::new_with_range(
                gtk::Orientation::Horizontal,
                range.min.into(),
                range.max.into(),
                range.step.unwrap_or(1).into(),
            );
            widget.connect_value_changed(move |widget| on_update(DeviceOptionValue::Int(widget.get_value() as i32)));
            widget.upcast()
        }
        C::FixedRange(range) => {
            let widget = gtk::Scale::new_with_range(
                gtk::Orientation::Horizontal,
                range.min.into(),
                range.max.into(),
                range.step.unwrap_or(SaneFixed::from_num(1)).into(),
            );
            widget.connect_value_changed(move |widget| on_update(DeviceOptionValue::Fixed(SaneFixed::from_num(widget.get_value()))));
            widget.upcast()
        }
        C::IntWhitelist(whitelist) => {
            let whitelist2 = whitelist.clone();
            option_widget_whitelist(whitelist.into_iter().map(ToString::to_string), move |i| on_update(DeviceOptionValue::Int(whitelist2[i])))
        },
        C::FixedWhitelist(whitelist) => {
            let whitelist2 = whitelist.clone();
            (option_widget_whitelist(whitelist.into_iter().map(ToString::to_string), move |i| on_update(DeviceOptionValue::Fixed(whitelist2[i]))))
        }
        C::StringWhitelist(whitelist) => {
            let whitelist2 = whitelist.clone();
            (option_widget_whitelist(
                whitelist
                    .into_iter()
                    .map(|x| x.to_string_lossy().into_owned()),
                move |i| on_update(DeviceOptionValue::String(whitelist2[i].clone()))
            ))
        }
        // _ => (
        //     true,
        //     gtk::Label::new(Some("(unsupported field, sorry)")).upcast(),
        // ),
    }
}

fn option_widget_read_value(widgets: &[gtk::Widget], value: DeviceOptionValue) {
    match value {
        DeviceOptionValue::NoValue => {}
        DeviceOptionValue::Bool(value) => {
            for widget in widgets {
                widget
                    .downcast_ref::<gtk::ToggleButton>()
                    .unwrap()
                    .set_active(value);
            }
        }
        DeviceOptionValue::Fixed(values) => {
            for (widget, value) in widgets.into_iter().zip(values) {
                let value_str = value.to_string();
                if let Some(widget) = widget.downcast_ref::<gtk::Entry>() {
                    widget.set_text(&value_str);
                } else if let Some(widget) = widget.downcast_ref::<gtk::ComboBox>() {
                    widget.set_active_id(Some(&value_str));
                } else if let Some(widget) = widget.downcast_ref::<gtk::Scale>() {
                    widget.set_value(value.to_num());
                } else {
                    println!(
                        "Tried to set unknown widget {:?} to fixed {:?}",
                        widget, value
                    );
                }
            }
        }
        DeviceOptionValue::Int(values) => {
            for (widget, value) in widgets.into_iter().zip(values) {
                let value_str = value.to_string();
                if let Some(widget) = widget.downcast_ref::<gtk::Entry>() {
                    widget.set_text(&value_str);
                } else if let Some(widget) = widget.downcast_ref::<gtk::ComboBox>() {
                    widget.set_active_id(Some(&value_str));
                } else if let Some(widget) = widget.downcast_ref::<gtk::Scale>() {
                    widget.set_value(value as f64);
                } else {
                    println!(
                        "Tried to set unknown widget {:?} to int {:?}",
                        widget, value
                    );
                }
            }
        }
        DeviceOptionValue::String(value) => {
            for widget in widgets {
                let value = value.to_string_lossy();
                if let Some(widget) = widget.downcast_ref::<gtk::Entry>() {
                    widget.set_text(&value);
                } else if let Some(widget) = widget.downcast_ref::<gtk::ComboBox>() {
                    widget.set_active_id(Some(&value));
                } else {
                    println!(
                        "Tried to set unknown widget {:?} to string {:?}",
                        widget, value
                    );
                }
            }
        }
    }
}
